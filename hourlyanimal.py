#!/usr/bin/python3
import tweepy
import tweepyauth

import jstyleson
import os
import random
from time import sleep
import logging



config_backup = '{\n	\n	"directory": "pictures",\n	// the directory the animal pictures are stored in\n	// will assume this directory is in the same directory as the .py\n\n	"frequency": 60,\n	// how often the bot posts in minutes\n\n	"include_filenames": false,\n	// if file names should be put as text in the tweet\n\n	\n}'


def getPicture(directory='./pictures/'):
	# get the list of image files in the directory -> pick a random one -> return it
	# since this will only execute every hour i dont care too much about reading the list every,
	# especially since you can then add more pictures without requiring a reboot
	pictures = []
	if not directory.endswith('/'): directory = f'{directory}/'

	for item in os.listdir(directory):
		item = f'{directory}{item}'
		if os.path.isfile(item):
			if item.endswith('.png') or item.endswith('.jpeg') or item.endswith('.jpg') or item.endswith('.gif'):
				# makes sure its an image file twitter supports
				pictures.append(item)
	return random.choice(pictures)

		


def readConfig(filename='config.json'):
	# function to get the json from the config file
	# i use jstyleson for this because it lets you add comments in json which makes
	# very easy to make human-readable config files without the need for configparser
	try:
		with open(filename,'r') as f:
			logging.info('read config')
			return jstyleson.loads(f.read())

	except FileNotFoundError as e:
		# if the file doesnt exist it load the config_backup 
		# it will also try to write the backup to the config file
		logging.warning(f'couldnt read {filename}: {e}')
		logging.warning('using default configuration instead.')
		try:
			with open(filename,'w') as ff:
				ff.write(config_backup)
		except:
			pass
		return jstyleson.loads(config_backup)


def main(config):
	logging.info('starting main loop.')
	
	while True:
		logging.info('preparing new post')

		picture = getPicture(config['directory'])
		logging.info(f'selected picture "{picture}"')

		logging.info('making tweet...')


		tweettext = ''
		if config['include_filenames']:
			tweettext = picture.split('/')[-1]

		tweet = api.update_with_media(picture,status=tweettext)

		logging.info(f'tweet posted to "https://twitter.com/{me.screen_name}/status/{tweet.id_str}"')

		logging.info(f'waiting for {config["frequency"]}m')
		sleep(config['frequency']*60)




if __name__ == '__main__':
	# set up logging
	logging.basicConfig(filename='log.txt',filemode='a',format='%(asctime)s %(levelname)-8s [%(funcName)s:%(lineno)-3s] %(message)s', datefmt='%H:%M:%S',level=logging.INFO)

	# get the path the .py is in
	# we do this to ensure it can be launched from any directory and still work properly
	pypath = os.path.abspath(__file__).replace(__file__.split('/')[-1],'')
	logging.info(f'using directory: "{pypath}"')

	# authenticate with twitter
	api = tweepyauth.auto_authenticate()
	if api == None:
		# we exit if this fails since the bot isnt much of a bot without twitter
		logging.critical('Could not authenticate with twitter')
		exit()
	me = api.me()
	logging.info(f'logged in as @{me.screen_name}')

	config = readConfig(f'{pypath}config.json')
	config['directory'] = f'{pypath}{config["directory"]}'
	main(config)


